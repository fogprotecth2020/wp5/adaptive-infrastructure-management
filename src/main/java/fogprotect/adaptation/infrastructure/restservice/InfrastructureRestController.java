package fogprotect.adaptation.infrastructure.restservice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

import fogprotect.adaptation.infrastructure.models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
public class InfrastructureRestController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @Autowired
    private RestTemplateBuilder restTemplateBuilder;


    // TESTING

    @GetMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

    // Custom Infrastructure Management Calls (not specified in API atm)

    @GetMapping("/currentInfrastructure")
    public Infrastructure infrastructure() {
        Infrastructure testObject = new Infrastructure();

        testObject.setNodeID(1337);
        testObject.setComponentName("TextComponent");
        testObject.setCpuCount(4);
        testObject.setStorageGB(1000);
        testObject.setSecureEnclave(true);

        return testObject;
    }

    // ------------------------------------------------------------
    // Implementation of requests based on latest API sepcification
    // ------------------------------------------------------------

    // --------
    // INCOMING

    @PostMapping(path = "/callForAction", consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity callForAction(@RequestBody Map<String, Object> payload) {
        //code
        if (payload == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }

        System.out.println("JSON payload: \n" + payload);

        return ResponseEntity.ok("Call for Action will be handled...");
    }

    @PostMapping(path = "/adaptationRequest", consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity adaptationRequest(@RequestBody Map<String, Object> payload) {
        //code
        if (payload == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }

        System.out.println("JSON payload: \n" + payload);

        // for now just wait for two seconds, then send the proposedAdaptation Request to the Coordinator
        //TODO: implement adaptation logic
        boolean successfullySentToCoordinator = sendProposedAdaptations();

        if (successfullySentToCoordinator) {
            return ResponseEntity.ok("Adaptations will be calculated...");
        }

        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);

    }

    @PostMapping(path = "/immediateAction", consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity immediateAction(@RequestBody Map<String, Object> payload) {
        //code
        if (payload == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }

        System.out.println("JSON payload: \n" + payload);

        return ResponseEntity.ok("Immediate Action successfully triggered");
    }

    // mocking coordinators proposedAdaptation endpoint
    @PostMapping(path = "/proposedAdaptation", consumes = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public ResponseEntity proposedAdaptation(@RequestBody ProposedAdaptations payload) {
        //code
        if (payload == null) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(null);
        }

        System.out.println("JSON payload: \n" + payload);

        return ResponseEntity.ok("Proposed Adaptation received");
    }

    // END INCOMING
    // ------------

    // --------
    // OUTGOING

    public boolean sendProposedAdaptations() {
        //String url = "http://localhost:8083/proposedAdaptation";
        String url = "http://192.168.1.248:7770/fogprotect/adaptationCoordinator/proposedAdaptation";

        // create headers
        HttpHeaders headers = new HttpHeaders();
        // set `content-type` header
        headers.setContentType(MediaType.APPLICATION_JSON);
        // set `accept` header
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

        // create an adaptations object
        ArrayList<Adaptation> emptyList= new ArrayList<>();
        ProposedAdaptations adaptations = new ProposedAdaptations(123, emptyList);

        // build the request
        HttpEntity<ProposedAdaptations> entity = new HttpEntity<>(adaptations, headers);

        RestTemplate restTemplate = restTemplateBuilder.build();

        // send request
        ResponseEntity response = restTemplate.postForEntity(url, entity, String.class);

        // check response status code
        if (response.getStatusCode() == HttpStatus.OK) {
            return true;
        } else {
            return false;
        }
    }

    // END OUTGOING
    // ------------
}

package fogprotect.adaptation.infrastructure.models;

public class Infrastructure {

    private int nodeID;
    private String componentName;
    private double storageGB;
    private double cpuCount;
    private boolean secureEnclave;

    public int getNodeID() {
        return nodeID;
    }

    public void setNodeID(int nodeID) {
        this.nodeID = nodeID;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public double getStorageGB() {
        return storageGB;
    }

    public void setStorageGB(double storageGB) {
        this.storageGB = storageGB;
    }

    public double getCpuCount() {
        return cpuCount;
    }

    public void setCpuCount(double cpuCount) {
        this.cpuCount = cpuCount;
    }

    public boolean isSecureEnclave() {
        return secureEnclave;
    }

    public void setSecureEnclave(boolean secureEnclave) {
        this.secureEnclave = secureEnclave;
    }
}

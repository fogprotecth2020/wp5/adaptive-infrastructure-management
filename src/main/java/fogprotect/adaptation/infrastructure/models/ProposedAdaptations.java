package fogprotect.adaptation.infrastructure.models;

import java.io.Serializable;
import java.util.ArrayList;

public class ProposedAdaptations implements Serializable {

    private int ID;
    private ArrayList<Adaptation> Adaptations;

    public ProposedAdaptations(int i, ArrayList<Adaptation> adaptations) {
        this.ID = i;
        this.Adaptations = adaptations;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public ArrayList<Adaptation> getAdaptations() {
        return Adaptations;
    }

    public void setAdaptations(ArrayList<Adaptation> adaptations) {
        Adaptations = adaptations;
    }
}

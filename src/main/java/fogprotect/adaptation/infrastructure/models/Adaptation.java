package fogprotect.adaptation.infrastructure.models;

import java.io.Serializable;

public class Adaptation implements Serializable {

    private String Name;
    private String Description;
    private String TypeOfManagementComponent;

    public Adaptation() {

    }

    public Adaptation(String name, String description, String typeOfManagementComponent) {
        Name = name;
        Description = description;
        TypeOfManagementComponent = typeOfManagementComponent;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        this.Description = description;
    }

    public String getTypeOfManagementComponent() {
        return TypeOfManagementComponent;
    }

    public void setTypeOfManagementComponent(String typeOfManagementComponent) {
        TypeOfManagementComponent = typeOfManagementComponent;
    }
}

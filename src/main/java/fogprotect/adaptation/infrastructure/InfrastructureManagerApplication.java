package fogprotect.adaptation.infrastructure;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@SpringBootApplication
public class InfrastructureManagerApplication {

    public static void main(String[] args) {
        //SpringApplication.run(InfrastructureManagerApplication.class, args);

        SpringApplication app = new SpringApplication(InfrastructureManagerApplication.class);
        app.setDefaultProperties(Collections
                .singletonMap("server.port", "8083"));
        app.run(args);
    }
}
